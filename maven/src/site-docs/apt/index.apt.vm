 -----
 Maven Project Parent POM
 -----
 Benson Margulies
 Hervé Boutemy
 Karl Heinz Marbaise
 -----
 2014-11-13
 -----

~~ Licensed to the Apache Software Foundation (ASF) under one
~~ or more contributor license agreements.  See the NOTICE file
~~ distributed with this work for additional information
~~ regarding copyright ownership.  The ASF licenses this file
~~ to you under the Apache License, Version 2.0 (the
~~ "License"); you may not use this file except in compliance
~~ with the License.  You may obtain a copy of the License at
~~
~~   http://www.apache.org/licenses/LICENSE-2.0
~~
~~ Unless required by applicable law or agreed to in writing,
~~ software distributed under the License is distributed on an
~~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~~ KIND, either express or implied.  See the License for the
~~ specific language governing permissions and limitations
~~ under the License.

~~ NOTE: For help with the syntax of this file, see:
~~ http://maven.apache.org/doxia/references/apt-format.html

Maven Project Parent POM

    This POM is the common parent of all of the Maven components
    in the Apache Maven project. Most of its contents are
    pinning down version numbers of plugins. It does 
    provide minimal dependencyManagement for <<<org.codehaus.plexus:plexus-component-annotations>>>
    and <<<org.apache.maven.plugin-tools:maven-plugin-annotations>>>.

    This POM contains Maven developers information for the {{{./team-list.html}Project Team report}},
    sorted by role and id.
    See the LDAP extract for more accurate {{{http://people.apache.org/committers-by-project.html#maven}committers}}
    and {{{http://people.apache.org/committers-by-project.html#maven-pmc}PMC members}} lists. 

The <<<reporting>>> Profile

    This POM provides <<<reporting>>> profile for rendering documentation during site generation:

+-----+
mvn -Preporting site
+-----+

    See {{{./plugins.html}Plugins report}} for a list of configured report plugins.

Site Publication

    This POM prepares site publication to svnpubsub. Every inheriting POM needs to define <<<maven.site.path>>> property
    with relative path to <<<$\{project.artifactId}-LATEST>>> publication uri, and define <<<distributionManagement>>> to avoid
    automatic inheritance from parent:

+-----+
  <properties>
    <maven.site.path>xxx-archives/${project.artifactId}-LATEST</maven.site.path>
  </properties>
  <distributionManagement>
    <site>
      <id>apache.website</id>
      <url>scm:svn:https://svn.apache.org/repos/infra/websites/production/maven/components/${maven.site.path}</url>
    </site>
  </distributionManagement>
+-----+

    Once this is configured, the site is published with:

+-----+
mvn -Preporting site site:stage
mvn scm-publish:publish-scm
+-----+

    See {{{/developers/website/deploy-component-reference-documentation.html}deploying Maven components reference documentation}}
    for more information.

History

#if( $project.version.endsWith( "SNAPSHOT" ) )
    trunk: {{{http://svn.apache.org/viewvc/maven/pom/trunk/asf/pom.xml?view=markup}$project.version}} ({{{http://svn.apache.org/viewvc/maven/pom/trunk/maven/pom.xml?r1=HEAD&r2=1632923&diff_format=h}diff}})
#end

    As of version 21, this POM sets the Java source and target versions to 1.5. Thus, as any plugin (or other component)
    moved to version 21+ of this POM, it moves to requiring Java 1.5.

*--------------+------------+
|| <<Version>> || <<Release Date>> ||
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-26/pom.xml?view=markup}26}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-26/pom.xml?r1=HEAD&r2=1632922&diff_format=h}diff}}) | 2014-11-13 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-25/pom.xml?view=markup}25}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-25/pom.xml?r1=HEAD&r2=1582495&diff_format=h}diff}}) | 2014-10-22 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-24/pom.xml?view=markup}24}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-24/pom.xml?r1=HEAD&r2=1465498&diff_format=h}diff}}) | 2014-03-27 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-23/pom.xml?view=markup}23}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-23/pom.xml?r1=HEAD&r2=1371599&diff_format=h}diff}}) | 2013-01-21 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-22/pom.xml?view=markup}22}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-22/pom.xml?r1=HEAD&r2=1157980&diff_format=h}diff}}) | 2012-08-08 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-21/pom.xml?view=markup}21}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-21/pom.xml?r1=HEAD&r2=1134934&diff_format=h}diff}}) | 2011-08-18 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-20/pom.xml?view=markup}20}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-20/pom.xml?r1=HEAD&r2=1069626&diff_format=h}diff}}) | 2011-06-15 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-19/pom.xml?view=markup}19}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-19/pom.xml?r1=HEAD&r2=1038256&diff_format=h}diff}}) | 2011-02-15 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-18/pom.xml?view=markup}18}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-18/pom.xml?r1=HEAD&r2=1029850&diff_format=h}diff}}) | 2010-11-26 |
*--------------+------------+
| {{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-17/pom.xml?view=markup}17}} ({{{http://svn.apache.org/viewvc/maven/pom/tags/maven-parent-17/pom.xml?r1=HEAD&r2=905048&diff_format=h}diff}}) | 2010-11-04 |
*--------------+------------+
